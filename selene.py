#Selene, yet another moon lander game
#See LICENSE for licensing information 

import sys, pygame
pygame.init()

window_size = width, height = 640, 480
speed = [2, 2]
black = 0, 0, 0

screen = pygame.display.set_mode(window_size)

lander = pygame.image.load("images/lander.png").convert()
lander_rect = lander.get_rect()

while 1:
  for event in pygame.event.get():
    if event.type == pygame.QUIT: sys.exit()
  lander_rect = lander_rect.move(speed)
  if lander_rect.left < 0 or lander_rect.right > width:
    speed[0] = -speed[0]
  if lander_rect.top < 0 or lander_rect.bottom > height:
    speed[1] = -speed[1]

  screen.fill(black)
  screen.blit(lander, lander_rect)
  pygame.display.flip()
